//console.log("Hello World!");

//Number 1

let num = Number(prompt("Give me a number"));
console.log("The number you provided is " + num + ".");

for (n = num; n>=0; n--){

	if(n==50){
		console.log("The current value is at 50. Terminating the loop.");
	}

	if(n<=50){
		break;
	}

	if(n % 10 == 0 && n!=0){
		console.log("The number is divisible by 10. Skipping the number");
		continue;
	}

	if(n % 5 == 0 && n!=0){
		console.log(n);
	}

	
}


//Number 2

let myString = "supercalifragilisticexpialidocious";
let myNewString= "";


for (let i=0; i<myString.length; i++){
		if(
			myString[i].toLowerCase() == "a" ||
			myString[i].toLowerCase() == "e" ||
			myString[i].toLowerCase() == "i" ||
			myString[i].toLowerCase() == "o" ||
			myString[i].toLowerCase() == "u" 		
	){
			continue;

	} else {
		myNewString += myString[i];
	}
}
	console.log(myString);
	console.log(myNewString);
