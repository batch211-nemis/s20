//console.log("Hello World");

//Repetition Control Structure (Loops)
	//one of the most important feature that programming that must have
	//It lets us execute code repeatedly in a pre-set number or mabe

	//Mini Activity

	function greeting(){
		console.log("Hi, Batch 211!");
	}
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();
	greeting();

	//or we can just loops

	let countNum = 10;

	while(countNum !== 0){
		console.log("This is printed inside the loop " + countNum);
		greeting();
		countNum--;
	}

	//While Loop
		//takes in an expression/condition
		//If the condition evaluates to true, the statements inside the code block will be executed

		/*
			Syntax:
				while(expression/condition){
					statement/code block
					final expression ++/--(iteration)
				}

				-expression/condition - this is the unit of code that is being evaluated in our loop. Loop will run while the statement is true
				-statement/code block - code/instructions that will be executed several times
				-finalExpression - indicates how to advance the loop
		*/

		let count = 5;
		//while the value of count is not equal to 0
		while(count !==0){
			//the current value of count is printed out
			console.log("While: " + count);
			count--; //decreses the value of count by 1 after every iteration to stop the loop when it reaches 0
		}

		/*let num = 20;

		while(num !== 0){
			console.log("While: num " + num);
			num--;
		}*/

		/*let digit = 5;
		while(digit !==20){
			console.log("While digit " + digit);
			digit++;
		}
*/

		/*let num1 = 1;
		while(num1 === 2){
			console.log("While: num1 " + num1);
			num--;
		}*/ //will not run because num1 is not equal to 2

		//Do-While Loop

		/*
			-works a lot like the while loop. But unlike while loop, do-while loops guarantee that the code will be executed at least once.

			Syntax:

				do  {
					statement/code block
					finalExpression ++/--
				} whilw (expression/condition)
		*/

		/*let number = Number(prompt("Give me a number"));
		do {
			console.log("Do while: " + number);
			number += 1;
		} while(number<10);*/

		//For Loop
			//is more flexible than while and do-while loops
			/*It consists of 3 parts:
				1. Initialization value that will track the progression of the loop
				2. Expression/Condition that will be evaluated which will determine if the loop will run one more time
				3.finalExpression inidcates how to advance the loop

			Syntax:

				for(initialization; expression/condition; finalExpression){
					statement
				}
			*/

			for (let count = 0; count <=20; count++){
				console.log("For loop: " + count);
			}

			//Mini Activity

			for (let count = 0; count <=20; count++){
				//console.log("For loop: " + count);
				if(count %2 == 0 && count != 0);{
					console.log("Even: " + count);
				}
			}

			let myString = "Camille Doroteo";
			//character in strings may be counted using the .length property
			//strings are special compared to other data types
			console.log(myString.length);

			//Accessing elements of a string
			//indivudual character of a string may be accessed using its index number
			//the first character in a string corresponds to number 0, the next is 1...

			console.log(myString[2]);
			console.log(myString[0]);
			console.log(myString[8]);
			console.log(myString[14]);

			for (let x = 0; x < myString.length; x++){
				console.log(myString[x])
			}

			//Mini- Activity

			let myFullName = "Roxanne Nemis";

			for (let x = 0; x < myFullName.length; x++){
				console.log(myFullName[x]);
			}

			let myName = "NehEmIAh";
			let myNewName = "";

			for (let i=0; i<myName.length; i++){
				if(
					myName[i].toLowerCase() == "a" ||
					myName[i].toLowerCase() == "e" ||
					myName[i].toLowerCase() == "i" ||
					myName[i].toLowerCase() == "o" ||
					myName[i].toLowerCase() == "u" 		
			){
					console.log(3);
				} else {
					console.log(myName[i]);
					myNewName += myName[i];
				}
			}
			console.log(myNewName);

		//Break and Continue Statements

		for (let count = 0; count<=20; count++){
			if (count %2 === 0){
				console.log("Even Number");
				continue;
			}
			console.log('Continue and Break: ' + count);

			if(count>10){
				break;
			}

		}

		let name = "alexandro";

		for (let i = 0; i<name.length; i++){
			console.log(name[i]);

			if(name[i].toLowerCase() === "a"){
				console.log("Continue to the next iteration");
				continue;
			}
			if(name[i].toLowerCase() == "d"){
				break;
			}
		}




